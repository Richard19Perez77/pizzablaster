package johns.pizza;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games;
import com.google.android.gms.plus.Plus;
import com.google.example.games.basegameutils.BaseGameUtils;

public class MainActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private final boolean isLogging = false;
    private final String TAG = "pizzaLog";

    public MediaPlayer mp = new MediaPlayer();
    public PizzaPanel pizzaPanel;
    public PizzaThread pizzaThread;
    public int soundStopped;

    boolean mResolvingConnectionFailure = false;
    boolean mAutoStartSignInFlow = true;
    boolean mExplicitSignOut = false;
    boolean mInSignInFlow = false; // set to true when you're in the middle of the
    // sign in flow, to know you should not attempt
    // to connect in onStart()
    private GoogleApiClient mGoogleApiClient;

    private static final int RC_SIGN_IN = 9001;
    private static final int HIGH_SCORE = 1;
    private static final int REQUEST_LEADERBOARD = 0;
    private int currentScore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (isLogging)
            Log.d(TAG, "onCreate Main Activity");
        getSupportActionBar().hide();
        setContentView(R.layout.activity_main);
        pizzaPanel = (PizzaPanel) findViewById(R.id.pizzaPanel);
        pizzaPanel.mp1 = mp;
        pizzaThread = pizzaPanel.getThread();
        pizzaPanel.onCreate();

        // Create the Google Api Client with access to the Play Games services
        if (isLogging)
            Log.d(TAG, "building client");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API).addScope(Plus.SCOPE_PLUS_LOGIN)
                .addApi(Games.API).addScope(Games.SCOPE_GAMES)
                        // add other APIs and scopes here as needed
                .build();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (isLogging)
            Log.d(TAG, "onStart Main Activity");
        if (isLogging)
            Log.d(TAG, "connect()");
        if (!mInSignInFlow && !mExplicitSignOut) {
            // auto sign in
            if (isLogging)
                Log.d(TAG, "auto sign in");
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isLogging)
            Log.d(TAG, "onResume Main Activity");
        // called after the application is ready to start
        if (soundStopped > 0) {
            mp.seekTo(soundStopped);
            mp.start();
        }
        pizzaPanel.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (isLogging)
            Log.d(TAG, "onActivityResult Main Activity");

        if (requestCode == RC_SIGN_IN) {
            if (isLogging)
                Log.d(TAG, "RC_SIGN_IN");
            mResolvingConnectionFailure = false;
            if (resultCode == RESULT_OK) {
                if (isLogging)
                    Log.d(TAG, "mGoogleApiClient.connect()");
                mGoogleApiClient.connect();
            } else {
                // Bring up an error dialog to alert the user that sign-in
                // failed. The R.string.signin_failure should reference an error
                // string in your strings.xml file that tells the user they
                // could not be signed in, such as "Unable to sign in."
                BaseGameUtils.showActivityResultError(this,
                        requestCode, resultCode, R.string.signin_failure);
            }
        } else if (requestCode == HIGH_SCORE) {
            if (isLogging)
                Log.d(TAG, "HIGH_SCORE");

            if (isLogging)
                Log.d(TAG, "onActivityResult pizzaPanel.score=" + pizzaPanel.finalScore);
            if (pizzaPanel.finalScore > currentScore)
                currentScore = pizzaPanel.finalScore;

            if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
                if (isLogging)
                    Log.d(TAG, "Games.Leaderboards.submitScore()");
                // Call a Play Games services API method, for example:
                uploadCurrentScore();
            }
            pizzaPanel.onActivityResult();
        }

        if (isLogging)
            Log.d(TAG, "call pizzaPanel.onRestart()");
        pizzaPanel.onRestart();
    }

    private void uploadCurrentScore() {
        if (currentScore > 0) {
            if (isLogging)
                Log.d(TAG, "submitScore=" + currentScore);
            Games.Leaderboards.submitScore(mGoogleApiClient, getString(R.string.leaderboard_high_score),
                    currentScore);
            currentScore = 0;
        }

        startActivityForResult(Games.Leaderboards.getLeaderboardIntent(mGoogleApiClient,
                getString(R.string.leaderboard_high_score)), REQUEST_LEADERBOARD);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (isLogging)
            Log.d(TAG, "onPause Main Activity");

        pizzaPanel.onPause();
        pizzaPanel.getThread().pause();

        if (mp != null) {
            if (mp.isPlaying()) {
                mp.pause();
                soundStopped = mp.getCurrentPosition();
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (isLogging)
            Log.d(TAG, "onStop Main Activity");

        if (isLogging)
            Log.d(TAG, "disconnect mGoogleApiClient");
        mGoogleApiClient.disconnect();

        pizzaPanel.onStop();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (isLogging)
            Log.d(TAG, "onRestart Main Activity");
        pizzaPanel.onRestart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isLogging)
            Log.d(TAG, "onDestroy Main Activity");
        if (mp != null) {
            if (mp.isPlaying())
                mp.stop();
            mp.release();
            mp = null;
        }

        pizzaPanel.onDestroy();
    }

    @Override
    public void onConnected(Bundle bundle) {
        if (isLogging)
            Log.d(TAG, "onConnected");
        //send score if saved from last activity result but not connected
        uploadCurrentScore();
    }

    @Override
    public void onConnectionSuspended(int i) {
        if (isLogging)
            Log.d(TAG, "onConnectionSuspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (isLogging)
            Log.d(TAG, "onConnectionFailed");
        if (mResolvingConnectionFailure) {
            if (isLogging)
                Log.d(TAG, "already resolving");
            // already resolving
            return;
        }

        // if the sign-in button was clicked or if auto sign-in is enabled,
        // launch the sign-in flow
        if (mAutoStartSignInFlow) {
            if (isLogging)
                Log.d(TAG, "mSignInClicked || mAutoStartSignInFlow");
            mAutoStartSignInFlow = false;
            mResolvingConnectionFailure = true;

            // Attempt to resolve the connection failure using BaseGameUtils.
            // The R.string.signin_other_error value should reference a generic
            // error string in your strings.xml file, such as "There was
            // an issue with sign-in, please try again later."
            if (!BaseGameUtils.resolveConnectionFailure(this,
                    mGoogleApiClient, connectionResult,
                    RC_SIGN_IN, getString(R.string.signin_other_error))) {
                if (isLogging)
                    Log.d(TAG, "!BaseGameUtils.resolveConnectionFailure");
                mResolvingConnectionFailure = false;
            }
        }
        // Put code here to display the sign-in button
        mAutoStartSignInFlow = true;
    }
}
