package movables;

public class LargePizza {

	public Pizza[] pieces;

	public LargePizza(int speed, int numPieces) {
		pieces = new Pizza[numPieces];
		for (int i = 0; i < numPieces; i++) {
			pieces[i] = new Pizza(speed);
			pieces[i].movementType = 0;
		}
	}
}
