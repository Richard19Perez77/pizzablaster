package movables;

import android.graphics.Bitmap;

public class Pizza {

	public int largex, largey;
	public int x, y, pizzaSpeed;
	public boolean exists;
	public int movementType;
	public int center;
	public boolean moveRight;
	public Bitmap piece;

	public Pizza(){

	}

	public Pizza(int speed) {
		y = -100;
		exists = false;
		pizzaSpeed = speed;
		movementType = (int) (Math.random() * 2);
		if (movementType == 1)
			moveRight = true;
	}

	public Pizza(int x, int y) {
		this.x = x;
		this.y = y;
		exists = false;
		pizzaSpeed = 1;
		movementType = (int) (Math.random() * 2);
		if (movementType == 2)
			moveRight = true;
	}

	public void movePizza(int incSpeed) {
		// speed helper is an offset to gradually increase or decrease speed

		switch (movementType) {
		case 0:
			center = x + 30;
			y += pizzaSpeed + incSpeed;
			break;
		case 1:
			if (moveRight) {
				x += 2;
				center = x + 30;
				y += pizzaSpeed + incSpeed;
			} else {
				x -= 2;
				center = x + 30;
				y += pizzaSpeed + incSpeed;
			}
			break;
		}
	}

	public void changeDirection() {
		//if var then !var & if !var then var is the same as
		//var = !var
		if (moveRight)
			moveRight = false;
		else
			moveRight = true;
	}
}
